FROM php:7.2-apache
RUN apt-get update && apt-get install -y curl build-essential libssl-dev zlib1g-dev libpng-dev libjpeg-dev libfreetype6-dev libicu-dev
RUN docker-php-ext-install intl
RUN docker-php-ext-configure intl
RUN docker-php-ext-install mysqli pdo pdo_mysql zip mbstring
RUN a2enmod rewrite
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install gd
COPY docker-php.conf /etc/apache2/conf-enabled/docker-php.conf    
COPY . /var/www/html
RUN service apache2 restart
